#pragma once 

#include "Row.h"

#include <iostream>
#include <iomanip>
#include <vector>

template < typename T >
class GraphType {
	public:
		GraphType();
		GraphType(const unsigned int size);
		bool is_empty() const;
		bool is_full() const;
		int get_index(const T& v) const;
		void add_vertex(const T& vertex);
		void add_edge(const T& v1, const T& v2, const int weight);
		int weight_of(const T& v1, const T& v2) const;
		std::vector<int> get_to_vertices(const T& v) const;
		void print() const;
		void empty();
		void clear_marks();
		void breath_first(const T& v1);
		std::vector< T >* depth_first(const T& v1, const T& v2);

	private:
		std::vector< T > m_vertices;
		std::vector< std::vector < int > > m_edges;
		std::vector< std::vector < bool > > m_visited;
		int max_size;

		void m_init(const unsigned int res_size);
		void m_empty();
		
};


template < typename T >
GraphType< T >::GraphType() {
	m_init(10);
}

template < typename T >
GraphType< T >::GraphType(const unsigned int size) {
	m_init(size);
}

template < typename T >
void GraphType< T >::m_init(const unsigned int size) {
	max_size = size;
	m_edges.resize(size, vector<int>(size, 0));
	m_visited.resize(size, vector<bool>(size, false));
}

template < typename T >
void  GraphType< T >::m_empty() {
	m_vertices.clear();
	m_edges.clear();
	m_visited.clear();
}

template < typename T >
bool GraphType< T >::is_empty() const {
	return m_vertices.empty();
}

template < typename T >
bool GraphType< T >::is_full() const {
	return m_vertices.size() == max_size;
}

template < typename T >
void GraphType< T >::add_vertex(const T& vertex) {
	if (!is_full()) {
		m_vertices.push_back(vertex);
	}
} 

template < typename T >
int GraphType< T >::get_index(const T& v) const{
	for (int i = 0; i < m_vertices.size(); i++) {
		if (m_vertices[i] == v) return i;
	}
	return -1;
}

template < typename T >
void GraphType< T >::add_edge(const T& v1, const T& v2, const int weight) {
	m_edges[get_index(v1)][get_index(v2)] = weight;
}

template < typename T >
int GraphType< T >::weight_of(const T& v1, const T& v2) const {
	return m_edges[get_index(v1)][get_index(v2)]
}

template < typename T >
std::vector<int>  GraphType< T >::get_to_vertices(const T& v) const {
	std::vector<int> index_list;
	const int index_of_v =get_index(v);
	
	for (int i = 0; i < m_vertices.size(); i++) {
		if (m_edges[index_of_v][i] && !m_visited[index_of_v][i]) {
			index_list.push_back(i);
		}
	}

	return index_list;
}

template < typename T >
void  GraphType< T >::print() const {
	for (size_t i = 0; i < m_vertices.size(); i++) {
		std::cout << std::setw(9) << m_vertices[i] << "  ";
	}
	std::cout << std::endl;

	for (size_t i = 0; i < m_vertices.size(); i++) {
		for (size_t j = 0; j < m_vertices.size(); j++) {
			std::cout << std::setw(9) << m_edges[i][j];
			if (m_visited[i][j]) {
				std::cout << "V  ";
			}
			else {
				std::cout << "   ";
			}
		}
		std::cout << std::endl;
	}
}

template < typename T >
void  GraphType< T >::empty() {
	m_empty();
	m_init(max_size);
}

template < typename T >
void  GraphType< T >::clear_marks() {
	m_visited.clear();
	m_visited.resize(max_size, std::vector<bool>(max_size, false));
}

template < typename T >
std::vector< T >* GraphType< T >::depth_first(const T& v1, const T& v2) {
	if (v1 == v2) { return new std::vector < T >(1, v2); }

	const int index_of_v1 = get_index(v1);
	std::vector<int> unvisited_index_list = get_to_vertices(v1);
	
	while (!unvisited_index_list.empty()) {
		m_visited[index_of_v1][unvisited_index_list.back()] = true;

		std::vector< T >* road = depth_first(m_vertices[unvisited_index_list.back()], v2);

		unvisited_index_list.pop_back();

		if (road) {
			road->push_back(v1);
			return road;
		}

	}
	return 0;
}

template < typename T >
void GraphType< T >::breath_first(const T& v) {
	clear_marks();

	Row<T> row;
	row.ennqueue(v);
	m_visited[0][get_index(v)] = true;

	while (!row.isEmpty()) {
		std::cout << row.front() << " ";
		

		std::vector<int> list = get_to_vertices(row.front());

		for (int i = 0; i < list.size(); i++) {
			if (!m_visited[0][list[i]]) {
				row.ennqueue(m_vertices[list[i]]);
				m_visited[0][list[i]] = true;
			}
		}

		row.dequeue();
	}
}
