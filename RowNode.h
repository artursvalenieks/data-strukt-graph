#ifndef ROWNODE_H
#define ROWNODE_H

template <typename Type>
struct RowNode{
public:
	RowNode() : next(this) {}
	RowNode(const RowNode<Type>& node) : value(node.value) {}
	RowNode(const Type& value, RowNode* next)
		: value(value), next(next) {}

	Type value;
	RowNode *next;
};

#endif // NODE_H
