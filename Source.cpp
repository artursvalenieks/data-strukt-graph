
#include "GraphType.h"

#include <iostream>
#include <string>

using namespace std;


int main() {
	
	
	GraphType<string>* cities = new GraphType<string>();
	
	cities->add_vertex("Austin");
	cities->add_vertex("Chicago");
	cities->add_vertex("Denver");
	cities->add_vertex("Dallas");
	cities->add_vertex("Washington");
	cities->add_vertex("Atlanta");
	cities->add_vertex("Houston");

	cities->add_edge("Austin", "Dallas", 200);
	cities->add_edge("Austin", "Houston", 160);
	cities->add_edge("Dallas", "Austin", 200);
	cities->add_edge("Dallas", "Chicago", 900);
	cities->add_edge("Dallas", "Denver", 780);
	cities->add_edge("Washington", "Atlanta", 600);
	cities->add_edge("Atlanta", "Washington", 600);
	cities->add_edge("Atlanta", "Houston", 800);
	cities->add_edge("Houston", "Atlanta", 800);
	cities->add_edge("Denver", "Atlanta", 1400);
	cities->add_edge("Denver", "Chicago", 1000);
	cities->add_edge("Chicago", "Denver", 1000);
	
	
	std::vector<string>* road = cities->depth_first("Austin", "Washington");
	
	while (!road->empty()) {
		std::cout << road->back() << std::endl;
		road->pop_back();
	}
	

	cities->print();

	cities->breath_first("Austin");

	cin.ignore();
	return 0;
}