#ifndef ROW_H
#define ROW_H

#include "RowNode.h"

class EmptyRowException {

};

template <class Type>
class Row
{
	public:
		Row() {
			m_frontNode = new RowNode<Type>();
			m_endNode = m_frontNode;
		}

		Row(const Row<Type> &row) {
			m_frontNode = new RowNode<Type>();
			m_endNode = m_frontNode;
			m_copyRow(row);
		}

		~Row() {
			empty();
		}

		Row<Type>& operator=(const Row<Type> &row) {
			if (this != &row) {
				empty();
				m_copyRow(row);
			}
			return *this;
		}

		void empty() {
			while (m_frontNode != m_endNode) {
				RowNode<Type>* oldNode = m_frontNode;
				m_frontNode = m_frontNode->next;
				delete oldNode;
			}
		}

		Type front() const {
			if (isNotEmpty()) {
				return m_frontNode->value;
			}
			else {
				throw new EmptyRowException;
			}
		}

		void ennqueue(Type value) {
			RowNode<Type>* newEndNode = new RowNode<Type>;
			m_endNode->next = newEndNode;
			m_endNode->value = value;
			m_endNode = m_endNode->next;
		}

		Type dequeue() {
			if (isNotEmpty()) {
				Type value = m_frontNode->value;
				RowNode<Type>* oldNode = m_frontNode;
				m_frontNode = m_frontNode->next;
				delete oldNode;
				return value;
			}
			else {
				throw new EmptyRowException;
			}
		}

		bool isNotEmpty() const {
			return !isEmpty();
		}

		bool isEmpty() const {
			return size() == 0;
		}

		unsigned int size() const {
			RowNode<Type> *runner = m_frontNode;
			int size = 0;
			while (runner != m_endNode) {
				size++;
				runner = runner->next;
			}
			return size;
		}

	private:
		RowNode<Type>* m_frontNode;
		RowNode<Type>* m_endNode;

		void m_copyRow(const Row<Type> &row) {
			for (RowNode<Type> *runner = row.m_frontNode;
					runner != row.m_endNode; runner = runner->next) {
				ennqueue(runner->value);
			}
		}
};


#endif // ROW_H

